package com.crazymakercircle.error;

import java.util.ArrayList;
import java.util.List;

class OOMThread {
    private final Byte[] toLeak;
    public OOMThread() {
        toLeak = new Byte[1024 * 1024];
    }
    // 为快速发生oom，设置堆大小; VM args: -Xms20m -Xmx20m
    public static void main(String[] args) throws InterruptedException {
        List<OOMThread> list = new ArrayList<>();
        Thread thread = new Thread(() -> {
            while (true) {
                list.add(new OOMThread());
            }
        });
        thread.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                e.printStackTrace();
                System.out.println("这里是没有捕获的处理 ====> " + t.getId() + "==> " + e.getLocalizedMessage());
            }
        });
        thread.start();
        while (true) {
            System.out.println(Thread.currentThread().getName() + " 我还行...");
            Thread.sleep(1000L);
            System.out.println(thread.getName() + " 的状态：" + thread.getState());
        }
    }
}

