package com.crazymakercircle.priority.threadpool;

import java.util.concurrent.*;

/**
 * 优先级线程池的实现
 *

 */
public class PriorityThreadPoolExecutor extends ThreadPoolExecutor {

  public PriorityThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit) {
    super(corePoolSize, maximumPoolSize, keepAliveTime, unit, buildWorkQueue());
  }
  public PriorityThreadPoolExecutor(int corePoolSize, int maximumPoolSize,
                   long keepAliveTime, TimeUnit unit, ThreadFactory threadFactory) {
    super(corePoolSize, maximumPoolSize, keepAliveTime, unit, buildWorkQueue(), threadFactory);
  }
  public PriorityThreadPoolExecutor(int corePoolSize, int maximumPoolSize,
                   long keepAliveTime, TimeUnit unit, RejectedExecutionHandler handler) {
    super(corePoolSize, maximumPoolSize, keepAliveTime, unit, buildWorkQueue(), handler);
  }
  public PriorityThreadPoolExecutor(int corePoolSize, int maximumPoolSize,
                   long keepAliveTime, TimeUnit unit, ThreadFactory threadFactory,
                   RejectedExecutionHandler handler) {
    super(corePoolSize, maximumPoolSize, keepAliveTime, unit, buildWorkQueue(), threadFactory, handler);
  }

  private static PriorityBlockingQueue buildWorkQueue() {
    return  new PriorityBlockingQueue(10, new PriorityTaskComparator());
  }

}