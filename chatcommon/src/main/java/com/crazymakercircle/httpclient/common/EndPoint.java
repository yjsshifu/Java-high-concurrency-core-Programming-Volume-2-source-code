package com.crazymakercircle.httpclient.common;

public interface EndPoint {
    void connect();

    void close();
}
